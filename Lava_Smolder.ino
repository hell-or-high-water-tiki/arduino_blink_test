// stolen from https://codebender.cc/sketch:86106#FadePulseGlow.ino
// commented by Stacie Barbarick
// 6/15/2022
// For Volcano Experience: Eruption At Hell or High Water Tiki Denver, CO


#include "FastLED.h"
#define NUM_LEDS 100  // # of LEDS in the strip
CRGB leds[NUM_LEDS];
#define PIN 6 // Output Pin to Data Line on Strip
#define COLOR_ORDER GRB  // I had to change this for my strip if your color is off then you know.
int fadeAmount = 5;  // Set the amount to fade 
int brightness = 0; 

void setup()
{
  FastLED.addLeds<WS2812B, PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
}

void loop()
{ 
   for(int i = 0; i < NUM_LEDS; i++ )
   {
   leds[i].setRGB(255,0,0);  // Set Color here. Red is selected
   leds[i].fadeLightBy(brightness);
  }
  FastLED.show();
  brightness = brightness + fadeAmount;
  // reverse the direction of the fading at the ends of the fade: 
  if(brightness == 0 || brightness == 255)
  {
    fadeAmount = -fadeAmount ; 
  }    
  delay(40);  // This delay sets speed of the fade. 
}
